<?php
return 'Found 4 models.
Inspecting model relations.

 1/4 [▓▓▓▓▓▓▓░░░░░░░░░░░░░░░░░░░░░]  25%
 3/4 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓░░░░░░░]  75%
 4/4 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%digraph "G" {
style="filled"
bgcolor="#F7F7F7"
fontsize="12"
labelloc="t"
concentrate="1"
splines="polyline"
overlap=""
nodesep="1"
rankdir="LR"
pad="0.5"
ranksep="2"
esep="1"
fontname="Helvetica Neue"
archixerdiagramtestsmodelsavatar:user_id -> archixerdiagramtestsmodelsuser:id [
label=" "
xlabel="BelongsTo
user"
color="#F77F00"
penwidth="1.8"
fontname="Helvetica Neue"
dir="both"
arrowhead="tee"
arrowtail="crow"
]
archixerdiagramtestsmodelscomment:id -> comment_user:comment_id [
label=" "
xlabel="BelongsToMany
user"
color="#003049"
penwidth="1.8"
fontname="Helvetica Neue"
]
comment_user:user_id -> archixerdiagramtestsmodelsuser:id [
label=" "
xlabel="BelongsToMany
user"
color="#003049"
penwidth="1.8"
fontname="Helvetica Neue"
]
archixerdiagramtestsmodelspost:user_id -> archixerdiagramtestsmodelsuser:id [
label=" "
xlabel="BelongsTo
user"
color="#F77F00"
penwidth="1.8"
fontname="Helvetica Neue"
dir="both"
arrowhead="tee"
arrowtail="crow"
]
archixerdiagramtestsmodelspost:id -> archixerdiagramtestsmodelscomment:post_id [
label=" "
xlabel="HasMany
comments"
color="#FCBF49"
penwidth="1.8"
fontname="Helvetica Neue"
dir="both"
arrowhead="crow"
arrowtail="none"
]
archixerdiagramtestsmodelsuser:id -> archixerdiagramtestsmodelspost:user_id [
label=" "
xlabel="HasMany
posts"
color="#FCBF49"
penwidth="1.8"
fontname="Helvetica Neue"
dir="both"
arrowhead="crow"
arrowtail="none"
]
archixerdiagramtestsmodelsuser:id -> archixerdiagramtestsmodelsavatar:user_id [
label=" "
xlabel="HasOne
avatar"
color="#D62828"
penwidth="1.8"
fontname="Helvetica Neue"
dir="both"
arrowhead="tee"
arrowtail="none"
]
archixerdiagramtestsmodelsuser:id -> comment_user:user_id [
label=" "
xlabel="BelongsToMany
comments"
color="#003049"
penwidth="1.8"
fontname="Helvetica Neue"
]
comment_user:comment_id -> archixerdiagramtestsmodelscomment:id [
label=" "
xlabel="BelongsToMany
comments"
color="#003049"
penwidth="1.8"
fontname="Helvetica Neue"
]
"archixerdiagramtestsmodelsavatar" [
label=<<table width="100%" height="100%" border="0" cellborder="1" cellspacing="0" cellpadding="10">
<tr width="100%"><td width="100%" bgcolor=""><font color="#333333">Avatar</font></td></tr>
</table>>
margin="0"
shape="rectangle"
fontname="Helvetica Neue"
]
"archixerdiagramtestsmodelscomment" [
label=<<table width="100%" height="100%" border="0" cellborder="1" cellspacing="0" cellpadding="10">
<tr width="100%"><td width="100%" bgcolor=""><font color="#333333">Comment</font></td></tr>
</table>>
margin="0"
shape="rectangle"
fontname="Helvetica Neue"
]
"archixerdiagramtestsmodelspost" [
label=<<table width="100%" height="100%" border="0" cellborder="1" cellspacing="0" cellpadding="10">
<tr width="100%"><td width="100%" bgcolor=""><font color="#333333">Post</font></td></tr>
</table>>
margin="0"
shape="rectangle"
fontname="Helvetica Neue"
]
"archixerdiagramtestsmodelsuser" [
label=<<table width="100%" height="100%" border="0" cellborder="1" cellspacing="0" cellpadding="10">
<tr width="100%"><td width="100%" bgcolor=""><font color="#333333">User</font></td></tr>
</table>>
margin="0"
shape="rectangle"
fontname="Helvetica Neue"
]
"comment_user" [
label=<<table width="100%" height="100%" border="0" cellborder="1" cellspacing="0" cellpadding="10">
<tr width="100%"><td width="100%" bgcolor=""><font color="#333333">Pivot</font></td></tr>
</table>>
margin="0"
shape="rectangle"
fontname="Helvetica Neue"
]
}
';
