<?php

namespace Archix\Erdiagram\Tests\Traits;

use Archix\Erdiagram\Tests\Models\Comment;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait HasComments
{
    /**
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

}
