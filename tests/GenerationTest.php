<?php

namespace Archix\Erdiagram\Tests;

use Illuminate\Support\Facades\Artisan;
use Spatie\Snapshots\MatchesSnapshots;

class GenerationTest extends TestCase
{
    use MatchesSnapshots;

    /** @test
     *
     */
    public function it_generated_graphviz_for_test_models()
    {
        // $this->markTestSkipped('need to fix generated snap sots');
        $this->app['config']->set('erdiagram.use_db_schema', false);
        $this->app['config']->set('erdiagram.directories', [__DIR__.'/Models']);

        Artisan::call('generate:erd', [
            '--format' => 'text'
        ]);

        $this->assertMatchesSnapshot(Artisan::output());
    }

    /** @test */
    public function it_generated_graphviz_for_test_models_with_db_columns_and_types()
    {
        $this->app['config']->set('erdiagram.directories', [__DIR__.'/Models']);

        Artisan::call('generate:erd', [
            '--format' => 'text'
        ]);

        $this->assertMatchesSnapshot(Artisan::output());
    }

    /** @test */
    public function it_generated_graphviz_for_test_models_with_db_columns()
    {
        $this->app['config']->set('erdiagram.use_column_types', false);
        $this->app['config']->set('erdiagram.directories', [__DIR__.'/Models']);

        Artisan::call('generate:erd', [
            '--format' => 'text'
        ]);

        $this->assertMatchesSnapshot(Artisan::output());
    }

    /** @test */
    public function it_generated_graphviz_in_jpeg_format()
    {
        $this->markTestSkipped('There is an error with my version of graphviz');

        $this->app['config']->set('erdiagram.directories', [__DIR__.'/Models']);

        Artisan::call('generate:erd', [
            '--format' => 'jpeg'
        ]);

        $this->assertContains('Wrote diagram to graph.jpeg', Artisan::output());
    }
}
