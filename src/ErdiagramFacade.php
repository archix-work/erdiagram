<?php

namespace Archix\Erdiagram;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Archix\Erdiagram\Skeleton\SkeletonClass
 */
class ErdiagramFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'erdiagram';
    }
}
