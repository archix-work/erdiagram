<?php

namespace Archix\Erdiagram\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static GRAPH()
 * @method static static BPM()
 * @method static static CGIMAGE()
 * @method static static DOT()
 * @method static static XDOT()
 * @method static static GIF()
 * @method static static GD()
 * @method static static GD2()
 * @method static static GTK()
 * @method static static ICO()
 * @method static static SVG()
 * @method static static PNG()
 * @method static static PSD()
 * @method static static JPEG()
 * @method static static JPE()
 * @method static static JPG()
 * @method static static JP2()
 * @method static static JSON()
 * @method static static DOT_JSON()
 * @method static static PDF()
 * @method static static PIC()
 * @method static static POV()
 * @method static static PS()
 * @method static static SGI()
 * @method static static WBMP()
 */
class GraphFormat extends Enum
{
    const  GRAPH    = 'graph';
    const  BPM      = 'bmp';
    const  CGIMAGE  = 'cgimage';
    const  DOT      = 'dot';
    const  XDOT     = 'xdot';
    const  GIF      = 'gif';
    const  GD       = 'gd';
    const  GD2      = 'gd2';
    const  GTK      = 'gtk';
    const  ICO      = 'ico';
    const  SVG      = 'svg';
    const  PNG      = 'png';
    const  PSD      = 'psd';
    const  JPEG     = 'jpeg';
    const  JPE      = 'jpe';
    const  JPG      = 'jpg';
    const  JP2      = 'jp2';
    const  JSON     = 'json';
    const  DOT_JSON = 'dot_json';
    const  PDF      = 'pdf';
    const  PIC      = 'pic';
    const  POV      = 'pov';
    const  PS       = 'ps';
    const  SGI      = 'sgi';
    const  WBMP     = 'wbmp';


}
