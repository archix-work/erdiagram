<?php

namespace Archix\Erdiagram\Console;


use Archix\Erdiagram\Domain\Model as GraphModel;
use Archix\Erdiagram\Graphviz\GraphBuilder;
use Archix\Erdiagram\Parsers\ModelFinder;
use Archix\Erdiagram\Parsers\RelationFinder;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use phpDocumentor\GraphViz\Exception;
use phpDocumentor\GraphViz\Graph;
use ReflectionClass;

class GenerateDiagramCommand extends Command
{
    const FORMAT_TEXT = 'text';

    const DEFAULT_FILENAME = 'graph';

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'generate:erd {filename?} {--format=png}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate ER diagram.';

    /** @var ModelFinder */
    protected ModelFinder $modelFinder;

    /** @var RelationFinder */
    protected RelationFinder $relationFinder;

    /** @var Graph */
    protected Graph $graph;

    /** @var GraphBuilder */
    protected GraphBuilder $graphBuilder;

    /**
     * @param  ModelFinder  $modelFinder
     * @param  RelationFinder  $relationFinder
     * @param  GraphBuilder  $graphBuilder
     */
    public function __construct(ModelFinder $modelFinder, RelationFinder $relationFinder, GraphBuilder $graphBuilder)
    {
        parent::__construct();

        $this->relationFinder = $relationFinder;
        $this->modelFinder    = $modelFinder;
        $this->graphBuilder   = $graphBuilder;
    }

    /**
     * @throws Exception
     */
    public function handle()
    {
        if (!$this->checkRequirements()) {
            $this->output->warning('please install Graphviz this is required by this package');
        } else {
            $models = $this->getModelsThatShouldBeInspected();

            $this->info("Found {$models->count()} models.");
            $this->info("Inspecting model relations.");

            $bar = $this->output->createProgressBar($models->count());

            $models->transform(function ($model) use ($bar) {
                $bar->advance();

                return new GraphModel(
                    $model,
                    (new ReflectionClass($model))->getShortName(),
                    $this->relationFinder->getModelRelations($model)
                );
            });

            $graph = $this->graphBuilder->buildGraph($models);

            if ($this->option('format') === self::FORMAT_TEXT) {
                $this->info($graph->__toString());

                return;
            }

            $graph->export($this->option('format'), $this->getOutputFileName());

            $this->info(PHP_EOL);
            $this->info('Wrote diagram to '.$this->getOutputFileName());
        }
    }

    /**
     * @return bool
     */
    protected function checkRequirements(): bool
    {
        return is_executable('/usr/bin/dot') || is_executable('/usr/local/bin/dot');
    }

    /**
     * @return Collection
     */
    protected function getModelsThatShouldBeInspected(): Collection
    {
        $directories = config('erdiagram.directories');

        $modelsFromDirectories = $this->getAllModelsFromEachDirectory($directories);

        return $modelsFromDirectories;
    }

    /**
     * @param  array  $directories
     *
     * @return Collection
     */
    protected function getAllModelsFromEachDirectory(array $directories): Collection
    {
        return collect($directories)
            ->map(function ($directory) {
                return $this->modelFinder->getModelsInDirectory($directory)->all();
            })
            ->flatten();
    }

    /**
     * @return string
     */
    protected function getOutputFileName(): string
    {
        return $this->argument('filename') ?:
            static::DEFAULT_FILENAME.'.'.$this->option('format');
    }
}
