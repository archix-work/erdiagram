<?php

namespace Archix\Erdiagram\Parsers;

use Composer\Autoload\ClassMapGenerator;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\ParserFactory;
use ReflectionClass;

class ModelFinder
{

    /** @var Filesystem */
    protected $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @return Collection
     */
    public static function getModelClassList(): Collection
    {
        return collect(config('erdiagram.directories'))
            // filter valid paths...
            ->filter(fn($dir) => is_dir(base_path($dir)))
            // get all model directory subdirectories...
            ->map(fn($dir) => glob($dir, GLOB_ONLYDIR))
            // flatten array of array...
            ->flatten()
            // check again that all directories are valid...
            ->filter(fn($dir) => is_dir($dir) && file_exists($dir))
            // for each directory generate php composer class map...
            ->map(fn($dir) => // composer sdk get classmap from dir.
                // get all the class names and filter out only models that
                // have the AutoLookup::trait.
            collect(ClassMapGenerator::createMap($dir))
                // get classes only drop paths
                ->keys()
                // @formatter:off
                // filter only models that use the AutoLookup trait...
                ->filter(fn($model) => is_subclass_of($model, EloquentModel::class)
                    && !(new ReflectionClass($model))->isAbstract()

                // @formatter:on
                )
            )
            // flatten array of array...
            ->flatten();
    }

    /**
     * @param  string  $directory
     *
     * @return Collection
     */
    public function getModelsInDirectory(string $directory): Collection
    {
        $files = config('erdiagram.recursive') ?
            $this->filesystem->allFiles($directory) :
            $this->filesystem->files($directory);

        $ignoreModels    = array_filter(config('erdiagram.ignore', []), 'is_string');
        $whitelistModels = array_filter(config('erdiagram.whitelist', []), 'is_string');

        $collection = Collection::make($files)
            ->filter(fn($path) => Str::endsWith($path, '.php'))
            ->map(fn($path) => $this->getFullyQualifiedClassNameFromFile($path))
            ->filter(fn(string $className) => !empty($className)
                && is_subclass_of($className, EloquentModel::class)
                && !(new ReflectionClass($className))->isAbstract());

        if (!count($whitelistModels)) {
            return $collection->diff($ignoreModels)->sort();
        }

        return $collection->filter(fn(string $className) => in_array($className, $whitelistModels));
    }

    /**
     * @param  string  $path
     *
     * @return string
     */
    protected function getFullyQualifiedClassNameFromFile(string $path): string
    {
        $parser = (new ParserFactory())->create(ParserFactory::PREFER_PHP7);

        $traverser = new NodeTraverser();
        $traverser->addVisitor(new NameResolver());

        $code = file_get_contents($path);

        $statements = $parser->parse($code);
        $statements = $traverser->traverse($statements);

        // get the first namespace declaration in the file
        $root_statement = collect($statements)->first(function ($statement) {
            return $statement instanceof Namespace_;
        });

        if (!$root_statement) {
            return '';
        }

        return collect($root_statement->stmts)
                ->filter(function ($statement) {
                    return $statement instanceof Class_;
                })
                ->map(function (Class_ $statement) {
                    return $statement->namespacedName->toString();
                })
                ->first() ?? '';
    }


}
