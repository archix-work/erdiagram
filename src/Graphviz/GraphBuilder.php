<?php

namespace Archix\Erdiagram\Graphviz;

use Archix\Erdiagram\Domain\Model;
use Archix\Erdiagram\Domain\ModelRelation;
use Doctrine\DBAL\Schema\Column;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Collection;
use phpDocumentor\GraphViz\Graph;
use phpDocumentor\GraphViz\Node;
use ReflectionClass;
use ReflectionException;
use Throwable;

class GraphBuilder
{
    /** @var Graph */
    private Graph $graph;

    /**
     * @param $models
     *
     * @return Graph
     */
    public function buildGraph(Collection $models): Graph
    {
        $this->graph = new Graph();

        foreach (config('erdiagram.graph') as $key => $value) {
            $this->graph->{"set${key}"}($value);
        }

        $this->addModelsToGraph($models);

        return $this->graph;
    }

    /**
     * @param  Collection  $models
     */
    protected function addModelsToGraph(Collection $models): void
    {
        // Add models to graph
        $models->map(function (Model $model) {
            $eloquentModel = app($model->getModel());
            $this->addNodeToGraph($eloquentModel, $model->getNodeName(), $model->getLabel());
        });

        // Create relations
        $models->map(function ($model) {
            $this->addRelationToGraph($model);
        });
    }

    /**
     * @param  EloquentModel  $eloquentModel
     * @param  string  $nodeName
     * @param  string  $label
     */
    protected function addNodeToGraph(EloquentModel $eloquentModel, string $nodeName, string $label): void
    {
        $node = Node::create($nodeName);
        $node->setLabel($this->getModelLabel($eloquentModel, $label));

        foreach (config('erdiagram.node') as $key => $value) {
            $node->{"set${key}"}($value);
        }

        $this->graph->setNode($node);
    }

    /**
     * @param  EloquentModel  $model
     * @param  string  $label
     *
     * @return string
     *
     * @noinspection HtmlUnknownAttribute
     * @noinspection HtmlDeprecatedAttribute
     * @noinspection HtmlDeprecatedTag
     */
    protected function getModelLabel(EloquentModel $model, string $label): string
    {
        $table = '<<table width="100%" height="100%" border="0" margin="0" cellborder="1" cellspacing="0" cellpadding="10">'.PHP_EOL;
        $table .= '<tr width="100%"><td width="100%" bgcolor="'.config('erdiagram.table.header_background_color').'"><font color="'.config('erdiagram.table.header_font_color').'">'.$label.'</font></td></tr>'.PHP_EOL;

        if (config('erdiagram.use_db_schema')) {
            $columns = $this->getTableColumnsFromModel($model);
            foreach ($columns as $column) {
                $label = $column->getName();
                if (config('erdiagram.use_column_types')) {
                    $label .= ' ('.$column->getType()->getName().')';
                }
                $table .= '<tr width="100%"><td port="'.$column->getName().'" align="left" width="100%"  bgcolor="'.config('erdiagram.table.row_background_color').'"><font color="'.config('erdiagram.table.row_font_color').'" >'.$label.'</font></td></tr>'.PHP_EOL;
            }
        }

        $table .= '</table>>';

        return $table;
    }

    /**
     * @param  EloquentModel  $model
     *
     * @return array|Column[]
     */
    protected function getTableColumnsFromModel(EloquentModel $model)
    {
        try {
            $table            = $model->getConnection()->getTablePrefix().$model->getTable();
            $schema           = $model->getConnection()->getDoctrineSchemaManager($table);
            $databasePlatform = $schema->getDatabasePlatform();
            $databasePlatform->registerDoctrineTypeMapping('enum', 'string');

            $database = null;

            if (strpos($table, '.')) {
                list($database, $table) = explode('.', $table);
            }

            return $schema->listTableColumns($table, $database);
        } catch (Throwable $e) {
        }

        return [];
    }

    /**
     * @param  Model  $model
     */
    protected function addRelationToGraph(Model $model): void
    {
        $modelNode = $this->graph->findNode($model->getNodeName());

        /** @var ModelRelation $relation */
        foreach ($model->getRelations() as $relation) {
            $relatedModelNode = $this->graph->findNode($relation->getModelNodeName());

            if ($relatedModelNode !== null) {
                $this->connectByRelation($model, $relation, $modelNode, $relatedModelNode);
            }
        }
    }

    /**
     * @param  Model  $model
     * @param  ModelRelation  $relation
     * @param  Node  $modelNode
     * @param  Node  $relatedModelNode
     */
    protected function connectByRelation(
        Model $model,
        ModelRelation $relation,
        Node $modelNode,
        Node $relatedModelNode
    ): void {
        if ($relation->getType() === 'BelongsToMany') {
            $this->connectBelongsToMany($model, $relation, $modelNode, $relatedModelNode);

            return;
        }

        $this->connectNodes($modelNode, $relatedModelNode, $relation);
    }

    /**
     * @param  Model  $model
     * @param  ModelRelation  $relation
     * @param  Node  $modelNode
     * @param  Node  $relatedModelNode
     *
     * @return void
     */
    protected function connectBelongsToMany(
        Model $model,
        ModelRelation $relation,
        Node $modelNode,
        Node $relatedModelNode
    ): void {
        $relationName  = $relation->getName();
        $eloquentModel = app($model->getModel());

        /** @var BelongsToMany $eloquentRelation */
        $eloquentRelation = $eloquentModel->$relationName();

        if (!$eloquentRelation instanceof BelongsToMany) {
            return;
        }

        $pivotClass = $eloquentRelation->getPivotClass();

        try {
            /** @var EloquentModel $relationModel */
            $pivotModel = app($pivotClass);
            $pivotModel->setTable($eloquentRelation->getTable());
            $label      = (new ReflectionClass($pivotClass))->getShortName();
            $pivotTable = $eloquentRelation->getTable();
            $this->addNodeToGraph($pivotModel, $pivotTable, $label);

            $pivotModelNode = $this->graph->findNode($pivotTable);

            $relation = new ModelRelation(
                $relationName,
                'BelongsToMany',
                $model->getModel(),
                $eloquentRelation->getParent()->getKeyName(),
                $eloquentRelation->getForeignPivotKeyName()
            );

            $this->connectNodes($modelNode, $pivotModelNode, $relation);

            $relation = new ModelRelation(
                $relationName,
                'BelongsToMany',
                $model->getModel(),
                $eloquentRelation->getRelatedPivotKeyName(),
                $eloquentRelation->getRelated()->getKeyName()
            );

            $this->connectNodes($pivotModelNode, $relatedModelNode, $relation);
        } catch (ReflectionException $e) {
        }
    }

    /**
     * @param  Node  $modelNode
     * @param  Node  $relatedModelNode
     * @param  ModelRelation  $relation
     */
    protected function connectNodes(Node $modelNode, Node $relatedModelNode, ModelRelation $relation): void
    {
        $edge = Edge::create($modelNode, $relatedModelNode);
        $edge->setFromPort($relation->getLocalKey());
        $edge->setToPort($relation->getForeignKey());
        $edge->setLabel(' ');
        $edge->setXLabel($relation->getType().PHP_EOL.$relation->getName());

        foreach (config('erdiagram.edge') as $key => $value) {
            $edge->{"set${key}"}($value);
        }

        foreach (config('erdiagram.relations.'.$relation->getType(), []) as $key => $value) {
            $edge->{"set${key}"}($value);
        }

        $this->graph->link($edge);
    }
}
