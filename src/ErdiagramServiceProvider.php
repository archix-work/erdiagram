<?php

namespace Archix\Erdiagram;

use Archix\Erdiagram\Console\GenerateDiagramCommand;
use Illuminate\Support\ServiceProvider;

class ErdiagramServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'erdiagram');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'erdiagram');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('erdiagram.php'),
            ], 'config');

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/erdiagram'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/erdiagram'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/erdiagram'),
            ], 'lang');*/

            // Registering package commands.
//             $this->commands([]);

        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'erdiagram');

        // Register the main class to use with the facade
        $this->app->singleton('erdiagram', function () {
            return new Erdiagram;
        });
        $this->app->bind('command.generate:diagram', GenerateDiagramCommand::class);

        $this->commands([
            'command.generate:diagram',
        ]);
    }


}
