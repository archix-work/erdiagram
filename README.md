# Generate ER Diagram of Laravel models and their relationships.

[comment]: <> ([![Latest Version on Packagist]&#40;https://img.shields.io/packagist/v/archix/erdiagram.svg?style=flat-square&#41;]&#40;https://packagist.org/packages/archix/erdiagram&#41;)

[comment]: <> ([![Total Downloads]&#40;https://img.shields.io/packagist/dt/archix/erdiagram.svg?style=flat-square&#41;]&#40;https://packagist.org/packages/archix/erdiagram&#41;)

[comment]: <> (![GitHub Actions]&#40;https://github.com/archix/erdiagram/actions/workflows/main.yml/badge.svg&#41;)

**Requirement**

Please install Graphviz package this requires the DOT program to be installed
 
-`sudo apt install graphviz`


After installing package publish the config and add the model you wish to generate erd for. You can add model and their relationships to either the `whitelist` or `ignore` to ignore models or their relationships.

Credits
I would like to git credit to `beyondcode/erd-generator` who's package I took inspiration and code form...
```php 
[
    'ignore'           => [
        // User::class,
        // Project::class => [
        //     'client'
        // ]
    ],
        'whitelist'        => [
        // App\User::class,
        // App\Project::class,
    ],
    ....
]
```
## Installation

You can install the package via composer:

```bash
composer require archix/erdiagram
```

## Usage

```php
php artisan generate:erd 
```
or
```php
Artisan::call('generate:erd');
```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email jason.kristian@superdraft.com.au instead of using the issue tracker.

## Credits

-   [Jason Kristian](https://github.com/archix)


## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
